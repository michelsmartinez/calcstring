/*
* Michel Martinez - 2017
* Programa que faz calculos com uma String como base
* a ideia é fazer uma biblioteca e melhorar as funções
* com mais contas e operações simultâneas
*
* O uso do código é liberado, caso goste dê uma estrelinha :)
*/


#include <ESP8266WiFi.h>

String str="";
long int cont = 0;

void setup() {
  Serial.begin(115200);
  delay(400);
  Serial.println("Serial OK!");
}

void serialEvent() {
  delay(1);
  yield();
  while (Serial.available()) {
    char inChar = (char)Serial.read();
    if (inChar == '\n' || inChar == '\r' || inChar == 10 || inChar == 13) {
      int atual=millis();
      calcula(str,5);
      Serial.println("Calculou em: " + String(millis()-atual) + " milisegundos");
      str="";
      return; 
    } else str += String(inChar);
  }
  return;
}


float calcula(String formula, float x){
  String operando[10];
  int a=0;
  char operacao[5];
  float aux[5];
  
  for(int i=0; i<10; i++){
    operando[i]="";
    if(i<5){
      aux[i]=0;
      operacao[i]='+';
    }
  }

  for(int i=0; (i<=formula.length() && a<10); i++){
     if((formula[i]>='0' && formula[i]<='9') || formula[i]=='-') {operando[a]+=formula[i];}
     else {
      if(operando[a]=="") operando[a] = formula[i];
      else {a++; operando[a]=formula[i];}
      a++;
     }
  }
  a=0;
  for(int i=0; i<5; i++){
    aux[i]=((operando[a][0]=='x' || operando[a][0]=='x')?x:float(operando[a].toInt()>0?operando[a].toInt():0));
    operando[i]=(operando[a][0]>0?operando[a][0]:0);
    a=a+2;
  }
  
  float total=aux[0];
  for(int i=1; i<5; i++){
    total=func(total,aux[i],operacao[i-1]);
  }
  //Serial.println(String(total));
  return total;
}

float func(float arg1, float arg2, char op){
    switch (op)
      {
      case '+':
            return(arg1 + arg2);
            break;
      case '-':
            return(arg1 - arg2);
            break;
      case '*':
            return(arg1 * arg2);
            break;
      case '/':
            if (arg2==0)
                  return 0;
            return(arg1 / arg2);
            break;
      case '^':
            return(pow(arg1, arg2));
            break;
      default:
            return arg1;
      }
}


void loop(){
  serialEvent();
  if(millis() - cont >10000) {
    Serial.println("10 segundos, str=" + str);
    cont=millis();
  }
}
